'use strict';

/**
 * This module contain the <code>runModelsMigrations</code> script, which
 * reconstructs all the database <b>deleting all the current data on it</b>
 * Use it only on development mode or with dummy data.
 * @module Migrations/Reset
 *
 */

var server = require('../../server/server');
var ds = server.dataSources.mysql_ds;

/**
 * Run the proper migrations by loading automatically all the new custom models.
 * @author Marcos Barrera del Río <elyomarcos@gmail.com>
 * @returns {undefined}
 */
function runModelsMigrations() {
  var lbTables = [
    'User', 'AccessToken', 'ACL',
    'RoleMapping', 'Role',
    'Customer', 'Provider', 'Product',
  ];

  ds.automigrate(lbTables, function(er) {
    if (er) throw er;

    console.log(
      'Loopback tables [' + lbTables + '] created in ',
      ds.adapter.name
    );
    ds.disconnect();
  });
}

runModelsMigrations();
